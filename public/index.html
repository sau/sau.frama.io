<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	<meta name='description'
content="Home of the saugns project and SAU (Scriptable AUdio) language." />
	<meta name='keywords'
content="saugns, SAU language, Scriptable AUdio, scripting language, FM" />
	<link rel='stylesheet' href='style.css' type='text/css' title='Default' /><style type='text/css'>@import url("style.css");</style>
<title>SAU &amp; saugns</title>
</head>
<body>
<h1>The SAU language &amp; saugns program</h1>
<p><a href='#saugns'>saugns</a> is an audio generation program. Written in C, it implements the <a href='#SAU'>SAU</a> (Scriptable AUdio) language &ndash; a simple language for mathematical audio synthesis. SAU could also be called an audio programming language, or a music programming language, these being old and commonly used terms for something rather general.
</p>
<div class='widebar'>
<img src="images/sau-logo167x95t.png" alt=" " style="float: right; margin: 1em" />
<h2>Project and downloads</h2>
<ul>
<li><a href="https://codeberg.org/sau/saugns">On Codeberg</a>,
`<tt>git clone </tt><code>https://codeberg.org/sau/saugns.git</code>`</li>
<li><a href="https://framagit.org/sau/saugns">On Framagit</a> (mirror &ndash; but also <a href="https://framagit.org/sau/sau.frama.io">hosts this site</a>)</li>
<li><a href="https://github.com/saugns/saugns">On GitHub</a> (extra mirror)</li>
</ul>
<h2>Browse pages</h2>
<ul>
<li><a href='usage.html'>Command-line usage</a></li>
<li><a href='language.html'>SAU language overview</a></li>
<li><a href='examples.html'>Examples with online audio</a></li>
<li><a href='changes.html'>Versions &amp; change logs</a></li>
<li><a href='history.html'>History of the project</a></li>
<li><a href='questions.html'>Answered questions</a></li>
<li><a href='design.html'>Design and main ideas</a></li>
</ul>
</div>
<div class='header'>
<h2 id='saugns'>saugns program</h2>
<p>More on command-line usage <a href='usage.html'>here</a>.
</p>
</div>
<p>saugns (the Scriptable AUdio GeNeration System) is a command-line
parser and player for SAU (Scriptable AUdio) files and text strings.
It can also write audio from scripts to WAV files, Au streams, and raw data.
</p>
<p>This project is written in C, without dependencies beyond the
standard C library plus system audio support on the given
<a href='questions.html#Platforms'>platform</a>. (Only systematically tested
on x86/x86-64 on *BSD and Linux systems, because that's what I have and run.)
Small and fast, the program can also be used on old and underpowered laptops.
</p>
<p>The <a href="https://codeberg.org/sau/saugns/src/branch/stable/README.md">README</a> file describes how to build and install it. (If you're running one of the 4 major BSD systems, `<tt>make</tt>` should be enough to get it built.)
</p>
<h3 id='simple-tests'>Simple tests</h3>
<p>After building the program, a 1-second 440 Hz test tone can be produced by simply running:
</p>
<pre>./saugns -e "Wsin"</pre>
<p>A more interesting test to run after building may be the following,
for 10 seconds of "engine rumble" using PM (it should sound <a href='audio/engine_rumble-v3.mp3'>like this</a>):
</p>
<pre>./saugns -e "Wsin f137 t10 p[Wsin f10*pi p[Wsin r(4/3)(pi/3)]]"</pre>
<p>Or for 30 seconds of random chirp drum drumming, with randomly strong dynamics-shaping...
</p>
<pre>./saugns -e "\$seed=time() Wsin f33 p[Rxpe mt1 f5 a33] a0.r1[Rcub f5*2] t30"</pre>
<p>Further examples and test scripts <a href="https://codeberg.org/sau/saugns/src/branch/stable/examples">come with the program</a>,
along with a concise SAU language reference, as described in the README file.
(There's also a selection of examples with rendered audio available on the <a href='examples.html'>examples page</a>.)
</p>
<h3 id='saugns-licensing'>Licensing</h3>
<p>saugns is distributed under the terms of the
<a href="https://www.gnu.org/licenses/lgpl-3.0.en.html">GNU Lesser General Public License (LGPL), version 3</a> or later.
Some smaller parts are however more permissively licensed, and each such file
notes which license (e.g. the ISC license) it is provided under.
</p>
<p>This website is available under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 License</a> &ndash; except for CC0 public domain <a href='examples.html#licensing'>example scripts and rendered audio</a>.
</p>
<div class='header'>
<h2 id='SAU'>SAU language</h2>
<p>See the main broad <a href='language.html'>language overview</a>; for some specific uses, there's also more <a href='examples.html'>how-to examples</a>.
A more detail-focused language reference is in the <i><a href="https://codeberg.org/sau/saugns/src/branch/stable/sau/doc/README.SAU">README.SAU</a></i> file.
</p>
</div>
<p>SAU is a simple language for mathematical audio synthesis, deliberately without support for the use of pre-recorded samples; scripts can usually be thought of as audio files in a terse text format.
</p>
<p>While a longer-term aim is to give it greater expressiveness for use in writing electronic music, it is straightforward to use for sounds and soundscapes, with small scripts able to produce complex sounds. The language intentionally has a restricted form, and is not Turing complete, each script having a duration which can be calculated without truly running it.
</p>
<p>Audio generation can use the following types of audio generators and ways of configuring them:
</p>
<ul>
	<li><code>A</code> &ndash; <a href='language.html#ampg'>Amplitude generator</a>, produces amplitude offsets i.e. DC offsets</li>
	<li><code>N</code> &ndash; <a href='language.html#noiseg'>Noise generator</a>, various noise colors and distributions; maximum rate, no frequency control</li>
	<li><code>R</code> &ndash; <a href='language.html#rasg'>Rumble oscillator (a.k.a. random line segments oscillator)</a>, various modes and line types; this allows smooth noise, and modulation both of and by noisy signals</li>
	<li><code>W</code> &ndash; <a href='language.html#wosc'>Wave oscillator</a>, various wave types</li>
	<li>Parameter <a href='language.html#sweep'>sweeps</a>, ADSR envelopes, and <a href='language.html#modulation'>modulation</a> by connecting objects &ndash; for AM/RM, FM/PM, and more</li>
	<li>Options for variations on <a href='language.html#pdist'>phase distortion synthesis</a>, and <a href='language.html#pulsar'>pulsar synthesis</a></li>
	<li>An arbitrary number of generators can be used, and changes to their parameters <a href='language.html#timing'>laid out in time</a></li>
</ul>
<p>The language is a work in progress. Long-term goals include for its features to make it simple to write longer, more intricate sequences of sounds &ndash; such as music. As for the instructions, generator types, etc. available, the road towards something more full-featured focuses on building a small collection of flexible things, rather than a large one of many mostly alike.
</p>
<p>Presently, the main lack in the language features is that reusing sequences of parameter changes requires a lot of copying of script text. But if such tedium is tolerated, it is fully possible to write music. Some key features in popular music-making software, such as processing to add echo and reverb, is however missing, which limits the resulting sound.
</p>
<h3 id='saulang-changes'>Changes between versions</h3>
<p>The SAU language sometimes changes between versions of the saugns program.
Changes are listed both in the version tags and in a shorter <a href='changes.html#saulang'>language change log</a>.
</p>
<div class='header'>
<h2 id='project'>Project and plans</h2>
<p>The history is briefly described <a href='history.html'>here</a>.
</p>
</div>
<p>Some ideas for future development are mentioned on the
<a href="https://codeberg.org/sau/saugns/wiki">project wiki</a> and issues
on Codeberg. Among them, a shorter list includes:
</p>
<ul>
	<li>Language constructs for re-use of larger definitions, and of generated signals.</li>
	<li>Feature(s) for concise description of signal patterns, and in turn rhythm, melody.</li>
</ul>
<h2 id='contact'>Contact</h2>
<p>Any of the sites which the project is on can be used (the main ones being <a href="https://codeberg.org/sau">Codeberg</a> and <a href="https://framagit.org/sau">Framagit</a>). Feedback is welcome, and you can also reach me by email (I'm "joelkp" at tuta.io, and "joelkpettersson" at Gmail.com).
</p>
<p>I also have a <a href="https://joelkp.frama.io">personal website</a>
where I touch on related and other things.
</p>
<div class='footer'>Text available under <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons BY-SA 4.0" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>.
</div>
</body>
</html>
