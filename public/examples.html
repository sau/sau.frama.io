<!DOCTYPE html>
<html lang='en' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	<meta name='description'
content="SAU (Scriptable AUdio) language examples and rendered audio files." />
	<meta name='keywords'
content="saugns, SAU language, examples, audio files" />
	<link rel='stylesheet' href='style.css' type='text/css' title='Default' /><style type='text/css'>@import url("style.css");</style>
<title>Rendered SAU examples</title>
</head>
<body>
<nav><img src="images/sau-logo88x50.png" alt=" " class='rightico' />
Back to <a href='index.html'>main page</a>.
</nav>
<h1>SAU examples with rendered audio</h1>
<p>Example scripts for <a href='language.html'>the SAU language</a>, and audio files rendered from them <a href='usage.html'>using</a> the <a href='index.html#saugns'>saugns</a> program. For short files that produce wave or noise types, etc., these are uncompressed WAV files so that the signal can be accurately viewed in an audio editor. For longer and other kinds of examples, MP3 files are provided; the uncompressed originals can be retrieved by re-rendering audio from the scripts.
</p>
<p><em>These are not all the available examples;</em> a somewhat different collection <a href="https://codeberg.org/sau/saugns/src/branch/stable/examples">is included with the saugns program</a>. Even more can be found in the <a href="https://codeberg.org/sau/extra-scripts">extra-scripts</a> git repository. Rather, this page gives how-to pointers for designing sounds using main SAU features, beyond what's on the main <a href='language.html'>language page</a>. It also showcases some sounds for all visitors.
</p>
<p>If you want to hear more of a script which here has a shorter duration, often it's possible to just copy it, adjust the <code>t</code>-number part(s) &ndash; for example, if it says <code>t30</code>, make it <code>t10*60</code> for 10 minutes &ndash; and then re-render the audio. (Some scripts have no timed substeps, other scripts have timed parameter value changes or <a href='language.html#sweep'>sweeps</a>; the sound of tweaking <code>t</code>-values may differ depending on where and how they're used.) You can also try out tweaking scripts to add, change, or remove sweeps, and the use of <abbr title="low-frequency oscillator">LFO</abbr>s, in addition to simpler parameter changes.
</p>
<p>Note that seeds for randomized sounds (see <a href='language.html#generators'>notes on audio generators</a>) may differ between the audio for examples on these pages vs. rendered with the latest saugns version.
</p>
<div class='toc floatleft'>
<h2 id='contents'>Contents</h2>
<ul>
<li><a href='#noiseg'><code>N</code> (Noise generator) examples</a></li>
<li><a href='#rasg'><code>R</code> (Rumble/random line segments oscillator) examples</a><ul>
	<li><a href='#R-rumbly'>Rumbles &amp; blasts using <code>R</code></a></li>
	<li><a href='#R-drumming'>Drumming using <code>R</code></a></li>
	<li><a href='#R-scapes'>Soundscapes and pseudo-melodies</a></li>
	<li><a href='#R-noisy'>Noise-on-noise</a></li>
	<li><a href='#R-miscsounds'>Other sounds using <code>R</code></a></li>
	</ul>
</li>
<li><a href='#wosc'><code>W</code> (Wave oscillator) examples</a><ul>
	<li><a href='#W-selfpm-bass'>Self-PM bass sounds</a></li>
	<li><a href='#W-pulsar'>Pulsar synthesis sounds</a></li>
	</ul>
</li>
<li><a href='#licensing'>Licensing of examples</a></li>
</ul>
</div>
<div class='toc'>
<h2 id='subpages'>Subpages</h2>
<ul>
<li><a href='examples/r-drums.html'><code>R</code> drum collection</a></li>
<li><a href='examples/w-types.html'><code>W</code> wave types</a></li>
</ul>
</div>
<header>
<h2 id='noiseg' style='clear:both'><code>N</code> (Noise generator) examples</h2>
<p>See also the simple <a href='usage.html#test-signal'>test signal examples</a> on the <a href='usage.html'>usage page</a>.</p>
</header>
<p>The <a href='language.html#noiseg'><code>N</code> noise generator</a> isn't so musically interesting in itself, but for modulation purposes <code>N</code> can be handy as a way to add plain noise to a parameter. As mentioned on the language page, the result differs for using noise to do AM, FM, and PM. (Adding it to amplitude or phase usually sound fairly similar, <a href='language.html#general-fm'>while adding it to frequency</a> changes the color of the noise in the direction of red/brown noise.) An example can be seen among the <a href='#R-scapes'><code>R</code> soundscapes</a>.
</p>
<p>For adding plain noise, <code>N</code> supports many noise types of <code>R</code> (though somewhat different labels are used for <code>N</code> noise types compared to <code>R</code> modes), as well as plain red/brown noise.
</p>
<h2 id='rasg'><code>R</code> (Rumble/random line segments oscillator) examples</h2>
<p>The <a href='language.html#rasg'><code>R</code> rumble/random line segments oscillator</a> was invented for the SAU language, combining the value noise generator and through-zero "<abbr title="Any audio generator which can be used for FM or PM (frequency modulation, phase modulation) synthesis.">FM operator</abbr>". It can also be used for 1D Perlin noise, as well as some other noise distributions and variations on waveforms (including a plain naive oscillator mode). It is easily used for noisy sounds with a rumbly character, or to introduce some pseudo-random variation to timbre, tone, etc. by modulating things with it. It has many modes, and sounds from it can be sharp and noisy or mellow and rumbly, but it can require extra processing to get something in-between those extremes out of <code>R</code>.
</p>
<p>Examples below demonstrate some of its features and basic uses. It's often used together with <a href='language.html#wosc'><code>W</code> &ndash; the wave oscillator</a> &ndash; but not always; in carrier-and-modulator chains, both <code>R</code> and <code>W</code> can play either role (carrier or modulator) in any combination.
</p>
<h3 id='R-rumbly'>Rumbles &amp; blasts using <code>R</code></h3>
<p><strong>Test tone rumble.</strong> This is to the <code>R</code> oscillator what a <a href='#wtypes-sin'>440 Hz test tone</a> is to the ordinary <code>W</code> oscillator. The base frequency is likewise the default 440 Hz &ndash; bright for a rumble, and somewhat like a low-pass filtered white noise except more regular in waveform. The mode is the default <code>mu</code> (value noise based on uniform white noise).
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/Rcos.wav' /></audio>
	<a href='audio/ex-page/Rcos.wav'>WAV</a>
</div>
<pre>Rcos t3</pre>
<div class="imgframe">
<div class="imgborder">
	<img src="images/waveform/Rcos-mu_clip.png" alt="Waveform image" width=1636 height=76 />
</div>
</div>
<p><strong>Test tone Perlin noise.</strong> This is the 1D Perlin noise variation on the above test tone rumble, sounding thinner. The peak in the frequency spectrum has also moved up to the 440 Hz frequency, instead of 440 Hz being where roll-off changes from gentle to steeper. Violet noise mode (using <code>mv</code> instead of <code>mp</code>) sounds somewhat similar, but a little darker and rougher for the same choice of frequency, more like high-pass filtered white noise based rumble in that way. Perlin and violet noise modes can also be used at the same time (<code>mvp</code> or <code>mpv</code>) for extra-thin Perlin noise.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/Rcos-mp.wav' /></audio>
	<a href='audio/ex-page/Rcos-mp.wav'>WAV</a>
</div>
<pre>Rcos mp t3</pre>
<div class="imgframe">
<div class="imgborder">
	<img src="images/waveform/Rcos-mp_clip.png" alt="Waveform image" width=2200 height=72 />
</div>
</div>
<p><em>Various other waveform variations are possible</em>, e.g. using <code>Rlin</code> for some added odd harmonics, <code>mb</code> for binary noise based waveforms with flat amplitude fluctuations, or <code>mg</code> for the opposite more Gaussian amplitude fluctuation pattern. With all the line types and modes, there's many combinations &ndash; too many for a full table of them all to be useful (unlike the simpler <a href='examples/w-types.html'>wave type palette</a> for the <code>W</code> oscillator). However, more are described <a href="https://joelkp.frama.io/tech/ran-extra-types.html">in an article series on such noise types (and modulation using them) on Joel's personal web site</a> also giving a more technical desription of the <code>R</code> oscillator.
</p>
<p><strong>Deep rumble.</strong> Using <code>Rcos</code> with a low frequency (here 50 Hz) creates a deep rumble, suitable as an ambient sound (and similar to some differently-sourced rumbles in ambient tracks). Note that to hear this, you'll need speakers or headphones that can reproduce low frequencies.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/ambient_rumble.mp3' /></audio>
	<a href='audio/ex-page/ambient_rumble.mp3'>MP3</a>
</div>
<pre>Rcos f50 t15</pre>
<p>There's however also other ways to make rumble, like using a <code>Wsin</code> carrier and then using <code>Rcos</code> as an FM <a href='language.html#modulation'>modulator</a>. Some variations on that theme &ndash; simply tweaking the parameter values &ndash; can make for thunder-like, explosion-like, and other sounds.
</p>
<p><strong>Explosion.</strong> Here's a "large explosion" blast which also includes a nice exponential fade-out at the end. A detail for the fade-out part (though its absence would have been hidden quite well by the fade-out) is that the pitch of the main modulator is frozen at the same time as the fade-out <a href='language.html#sweep'>sweep</a> begins. (The pitch is determined by the sub-modulator 'p', and its frequency is set to 0 Hz, freezing its output.) <code>Rsmo</code> sounds similar to <code>Rcos</code> but creates a slightly brighter and more intense sound.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/kaboom1.mp3' /></audio>
	<a href='audio/ex-page/kaboom1.mp3'>MP3</a>
</div>
<pre>
Wsin a0[g1 lexp t1/4] f-500.r+500[
        Rsmo r1.r10[
                'p Wsin p0/4.a1/2 f1/5
        ]
] t5 /5 a[g0 lexp] t1.5 @p f0
</pre>
<p>The use of <a href='language.html#self-pm'>self-PM</a> in the innermost LFO modulator turns it into an envelope shape with a sharp attack and slower decay. This causes some phase drift though, adding to the need for a 1&frasl;4 second fade-in to accomodate the leading part. An older variation instead made a symmetriclly shaped "large fireball" sound (the innermost modulator looking like <code>'p Wsin p3/4 f1/5</code>), with no fade-in.
</p>
<p><strong>Cat purr.</strong> A very different kind of richer rumble &ndash; imitating cat purring &ndash; is also possible with <code>Rcos</code> or something similar as a carrier. <code>Rsmo</code> sounds a little brighter and is used here. This is a little more intricate, and uses a combination of AM (to shape the amplitude envelope, and brighten the sound), and a little PM (to make a somewhat richer sound). However, it sounds a little too dull compared to real cat purring.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/cat-purr.mp3' /></audio>
	<a href='audio/ex-page/cat-purr.mp3'>MP3</a>
</div>
<pre>
Rsmo f50 p[
        Wean r1 a1/2
] a1/3[
        Rsmo r1 a0[
                Wspa f1 a1/3
        ]
        Wspa f1 a1/3
] t30
</pre>
<header>
<h3 id='R-drumming'>Drumming using <code>R</code></h3>
<p>See the <a href='examples/r-drums.html'><code>R</code> drum collection</a> for more.</p>
</header>
<p>There's several ways to use <code>R</code> instances for drumming, but the main possibilities come from using it for modulation. In particular, using an <code>Rxpe</code> PM modulator to make a carrier "beat" like a drum is a key ingredient in a range of simple recipes; apart from that modulator, the sound greatly depends on the choice of carrier. A more polished sound also usually depends on adding modulation of the amplitude to the carrier &ndash; for example using another <code>R</code> instance.
</p>
<p>Looking first at the <code>Rxpe</code> PM modulator, intensity and pace of drumming can be constant or be psuedo-randomly varied, by tweaking its mode, frequecy, and amplitude. For a fixed-pace drumbeat, fixed cycle mode <code>mf</code> or ternary smooth mode <code>mt</code> can be used &ndash; otherwise it will be pseudo-random drumming, which can be given a variety of simple patterns (varying the brightness or intensity, the timing, or both). More variations are also possible by using several PM modulators with different settings for the same drum.
</p>
<p>Technically, these drum sounds contain two different types of drum beat events which sound the same or similar. The PM modulator may rise (sharply at first), or it may fall (sharply at first) &ndash; and both will brighten the carrier's sound in the same way. If a <a href='language.html#wosc'><code>W</code> wave oscillator</a> is used as the carrier, with its frequency set to 0 Hz, it will sound the same &ndash; but with an <code>R</code> oscillator as the carrier, it will normally sound a bit different, because <code>R</code> is pseudo-periodic and each part of its signal makes for a somewhat different drum hit.
</p>
<p>For a start, here's a simple drum script with named variables for quantities like <abbr title="beats per minute">bpm</abbr> and for "strength" (intensity and brightness) of the drum sound. The carrier has a very low frequency, rather than a frequency of 0 Hz &ndash; this helps vary the sound a little over time, instead of making it alternate between the exact same back-and-forth beats all the time. Otherwise, the <code>f$bpm/(60*2)</code> could be moved in to the modulator, with <code>f0</code> added to the carrier. (A somewhat higher carrier frequency can also be used to produce a background hum or bassy noise which alternates with the drumbeats. For example, reusing the 'strength' variable, as in <code>f$strength</code>.)
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/Rdrum-bpm.mp3' /></audio>
	<a href='audio/ex-page/Rdrum-bpm.mp3'>MP3</a>
</div>
<pre>
$bpm=240
$strength=100
Rlin f$bpm/120 p[
        Rxpe mf a$strength*(120/$bpm)
] t30
</pre>
<p>It's a start, but without more it sounds a little rough in several ways. The subjective audio quality can be improved, among possibly other and much larger changes, by tweaking the carrier. Even then, without also adding amplitude modulation, such drum sounds have flat peak amplitude, the sense of intensity change coming from frequency change only &ndash; which usually sounds too soft or flat to be musically nice.
</p>
<h3 id='R-scapes'>Soundscapes and pseudo-melodies</h3>
<p>Using <code>R</code> can easily create pseudo-melodies and (to a lesser extent) pseudo-rhythms, that is pseudo-random melodies and rhythms which simply keep going and developing until the time runs out. These will have relatively simple patterns and won't stick to the norms of harmonic composition, but can be nice enough as background sound. (Note that the simplest <code>R</code> randomized "synth instruments" trick of using an <code>R</code> carrier with a very low, single-digit Hz frequency, and then modulating it into making a bright synth soundscape, usually results in plenty of infrasonic frequencies remaining in the signal &ndash; a rumbly quality may or may not be audible.)
</p>
<p>The unusual "ternary smooth random" mode (<code>mt</code>) makes for a more harmonically pure and steady signal than other modes, apart from the fixed cycle mode <code>f</code>. This can make <code>R</code> oscillators usable as more deeply nested modulators, for producing a sharp and crisp sound, when even a small amount of noise in such a role otherwise leads to a more diffuse result, overpowered or at least diluted by noise. The following soundscape is a simple example; here, the Gaussian mode <code>mg</code> is also used for the <code>R</code> carrier instead of the uniform random default, as a simple way to get more loudness fluctuation to the sound.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/bg-scape-00.mp3' /></audio>
	<a href='audio/ex-page/bg-scape-00.mp3'>MP3</a>
</div>
<pre>
$f=10/3
Rcos mg f$f t60 p[
	Wsin r10 a4 p[Wsin r-2.r+2[Rlin mt p0/4 f20*$f]]
]
</pre>
<p>More harmonically rich line and wave types, like using <code>Rlin</code> and/or modulating with <code>Wtri</code>, can also sometimes result in pleasant sounds.
</p>
<p><strong>Use with <code>N</code>.</strong> The following script goes a step further, and uses <code>Nwh</code> to noisily stretch out the frequencies of the wave oscillator in the middle of the modulation chain to a larger range; why pick one frequency, or another, when each makes the sound a bit fuller in some way, when you can instead use noise to select <em>all</em> of them? For "real FM" as done here, this colors the sound with brown noise, not white noise, as a result of integration taking place inside the middle oscillator &ndash; so the result isn't harsh.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/bg-scape-02.mp3' /></audio>
	<a href='audio/ex-page/bg-scape-02.mp3'>MP3</a>
</div>
<pre>
Rlin f3 t60 p[Wtri r8.r32[Nwh] a2]
</pre>
<p><strong>Randomized "notes".</strong> While the above example has a random mixture of sounds in a range of them, it also stays quite flat in that it seems to stick to the same note. For a better pseudo-random "melody", <code>R</code> can be used instead of <code>N</code> as an inner FM modulator. But while it may seem intuitive to use <code>Rsah</code> to vary the frequency &ndash; resulting in squarish jumps between different frequencies &ndash; doing so sounds rather stiff. By contrast, <code>Ruwh</code> is a noisy line type (described <a href='#R-noisy'>further below</a>), adding varying amounts of white noise to the squarish steps, which sounds more organic or "fluid" in how frequency varies.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/bg-scape-02b.mp3' /></audio>
	<a href='audio/ex-page/bg-scape-02b.mp3'>MP3</a>
</div>
<pre>
Rlin f3 t60 p[Wtri r8.r32[Ruwh f1] a2]
</pre>
<p>This sounds quite soft, and for simple scripts like this, much in the sound will change with tweaks to line and/or wave type selection in carrier or outer modulator, and how they modulate. Here's a different sound, a preudo-random bass melody.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/bg-scape-02c.mp3' /></audio>
	<a href='audio/ex-page/bg-scape-02c.mp3'>MP3</a>
</div>
<pre>
Rlin f2 t60 p[Wmto r8.r64[Ruwh f1] a2]
</pre>
<p>Note that the non-quantized random values used to vary the frequency makes it hit notes outside the customary 12-tone scale. Simple scripts for this kind of pseudo-melody always end up microtonal.
</p>
<h3 id='R-noisy'>Noise-on-noise</h3>
<p>There's a simple way to produce more intricate, very bright and noisy sounds. While the usual <a href='language.html#line-types'>line types</a> result in a less bright spectrum above the <code>f</code> or <code>r</code> frequency used for an <code>R</code> instance, there is also a special noise line type &ndash; <code>uwh</code> (uniform white noise) &ndash; which fills the half-cycles with noise. The result also depends a lot on the <code>R</code> mode selection, which determines the differences in amplitude and DC offset between each noise chunk generated &ndash; the default mode gives such variation, others lessen or alter it in various ways.
</p>
<p>Modulation is key to making the resulting sounds more intricate &ndash; but adding one <a href='language.html#wosc'>wave oscillator</a> is enough to produce a variety of noise-scapes, be they static, morphing, or with some pseudo-melody (in large part depending on the carrier frequency). Such sounds with the <code>uwh</code> line are much brighter than similar sounds made using normal line types for the <code>R</code> carrier &ndash; but generally too rough-sounding for many musical uses, like pleasant synth sounds, at least without some filtering afterwards. Here's a somewhat melodious one-liner.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/bg-noise-00.mp3' /></audio>
	<a href='audio/ex-page/bg-noise-00.mp3'>MP3</a>
</div>
<pre>
Ruwh t60 f2 p[Whsi f30 p1/12 a2]
</pre>
<p>Switching the modulator to the <code>saw</code> wave type, and reducing the carrier frequency to a fraction would produce something else, sounding a bit more like low-fi electronics acting up.
</p>
<p>Noise from <code>Ruwh</code> can also be used to modulate another signal &ndash; and <a href='language.html#general-fm'>if FM rather than PM is used</a>, the result is more mild. <a href='#R-scapes'>Pseudo-melody examples above</a> use it as it's less stiff-sounding than using the pure squarish <code>Rsah</code> to flip between frequencies.
</p>
<h4>Noisy sound effects, softer and harder</h4>
<p>Apart from the full-on noise line type <code>uwh</code>, there's also two milder line-with-noise line types, which sound like the <code>lin</code> line type combined with a less intense white noise. This lesser noise is also limited in its bassiness, as it fades in and out over the line length.
</p>
<p>The middling noise line type is <code>nhl</code> &ndash; "noise hump" line &ndash; which adds a noise that grows in intensity towards the middle of the line. The mildest variation is <code>ncl</code> &ndash; the "noise camel" line, named after the two softer noise bulges it has &ndash; which also sounds a little smoother and less bassy.
</p>
<p>Here's a script using each of the 3 types for 5 seconds, from softer to harder &ndash; making a lot of little "blasts" which sound like something that may fit a 90s-style video game. (Some other related sound effects result if the modulator amplitude is changed, e.g. doubled.)
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/line_noisy.mp3' /></audio>
	<a href='audio/ex-page/line_noisy.mp3'>MP3</a>
</div>
<pre>
Rncl t15 f20 p[Wspa f30 a2] /5 lnhl /5 luwh
</pre>
<h3 id='R-miscsounds'>Other sounds using <code>R</code></h3>
<h4>Metallic babbling</h4>
<p>Vaguely speech-like soundscapes, which give the impression of a voice with an attitude while sounding very artificial, result from some simple combinations of a modulator and an <code>R</code> carrier. Here's three variations playing 8 seconds each.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/babbly-metallic.mp3' /></audio>
	<a href='audio/ex-page/babbly-metallic.mp3'>MP3</a>
</div>
<pre>
Rcos f50 t8 p[
        Wtri r1 a12.5 t8; r1/3
]; f120 t8; mt f200 t8
</pre>
<p>Here the <code>R</code> mode <code>mt</code> for the last part gives a different electric sound to it, where the higher frequency otherwise fits less well.
</p>
<h2 id='wosc'><code>W</code> (Wave oscillator) examples</h2>
<p>The oldest core feature is <a href='language.html#wosc'><code>W</code> &ndash; the wave oscillator</a> &ndash; which can be put to a great many uses through <a href='language.html#modulation'>modulation</a>. This section focuses mainly on <code>W</code> use, while other kinds of examples highlight features of other generators or effects (even if they're used together).
</p>
<p id='W-wtypes'>The <a href='examples/w-types.html'><code>W</code> wave types subpage</a> covers the basic waveforms of this oscillator, with audio for each. It also offers recipes for deriving other waveforms, including the use of <a href='language.html#pdist'>phase distortion synthesis</a> options for PWM and other things.
</p>
<h3 id='W-selfpm-bass'>Self-PM bass sounds</h3>
<p><a href='language.html#self-pm'>Phase self-modulation (a.k.a. "feedback FM")</a> can among other things be used to create a range of bass sounds, which sound somewhat like subtractive synthesizer sounds. In the following example, the strength of self-PM is modulated so that it's stronger at the peak of a sound and fades as the sound fades, making the sound mellower (less sawtooth-like).
</p>
<p>This script has variables for varying <abbr title="beats per minute">bpm</abbr>, pitch, and timbre brightness or "strength". For modulating the strength of self-PM applied, in place of using a downward sawtooth ramp, <code>Rsqe mfh</code> is used, as this gives a <em>squared</em> downward ramp, decaying faster.) For the amplitude, however, a gentler, more linear downward ramp is used &ndash; and to provide a smooth "attack" and "release" envelope rather than an instant rise, self-PM is used again to make the amplitude modulator rise fast and decay slowly (<code>p.a0.5</code> giving it a pretty sharp rounded sawtooth without ripple).
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/bass-sounds.mp3' /></audio>
	<a href='audio/ex-page/bass-sounds.mp3'>MP3</a>
</div>
<pre>
$bpm?=120
$strength?=2
$pitch?=3
Wean f$pitch*30 p.a$strength/8[
        Rsqe mfh f$bpm/60 a$strength/8
] a1/8.r1[
        Wtri p.a.5 f$bpm/60
] t5; wspa; wtri; wsin
</pre>
<p>This script has 4 &times; 5 second parts, switching wave type for the carrier: first <code>ean</code>, then <code>spa</code>, then <code>tri</code>, and finally <code>sin</code>. Each gives a distinct sound; the <code>ean</code> type in this context sounds similar to <code>sin</code>, except that it gives a somewhat fuller sound and (unintuitively, as a result of how the carrier and modulation combine psychoacoustically) an illusion of a faint reverb like the other non-sine wavetypes.
</p>
<p><strong><code>R</code> carrier "industrial" sound.</strong> A variation on the same script using <code>R</code> as a carrier can produce a rougher, richer bass sound. But most randomness modes sound bad for this &ndash; too rough! The best modes are the <a href="https://joelkp.frama.io/tech/ran-quasirandom.html">quasirandom</a> <code>t</code> (ternary smooth) and <code>a</code> (additive recurrence), where for the latter the default setting of using <code>ma.amet(1)</code> gives a rich timbre; various other values for the <code>m.a</code> subparameter tend to sound darker or smoother, though not all. Every 5 seconds, this example alternates between testing modes <code>t</code> and <code>a</code>, switching line type after every pair for 4 &times; 2 variations.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/bass-sounds2.mp3' /></audio>
	<a href='audio/ex-page/bass-sounds2.mp3'>MP3</a>
</div>
<pre>
$bpm?=120
$strength?=2
$pitch?=4
Rcos mt f$pitch*30 p.a$strength/8[
        Rsqe mfh f$bpm/60 a$strength/8
] a1/8.r1[
        Wtri p.a.5 f$bpm/60
] t5; ma; llin mt; ma; lsqe mt; ma; lnhl mt; ma
</pre>
<p>The pitch is set a notch higher in this version, as otherwise it sounds too dark &ndash; the spectrum extends down further anyway so it fits.
</p>
<h3 id='W-pulsar'>Pulsar synthesis sounds</h3>
<p><a href='language.html#pulsar'>Pulsar synthesis</a> morphs timbre (and can also create rhythm) by adding silence (short or long) to the sound at the small scale.
</p>
<p>Sounds like the following can be found in some electronica. In this case it uses a single cycle of a sine wave as the "pulsaret" sound grain which becomes surrounded by silence.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='audio/ex-page/pulwm-woeu.mp3' /></audio>
	<a href='audio/ex-page/pulwm-woeu.mp3'>MP3</a>
</div>
<pre>W.c1.r200[Wpar f1 R mp f1/2] f20.r100[R mb1p f2] t30
</pre>
<p>The above script separately LFO modules both frequency, and the degree of "zooming out" the sine wave cycle up to 200x using the <code>c</code> subparameter.
</p>
<h2 id='licensing'>Licensing of examples</h2>
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
	<a class='rightico'
		href="http://creativecommons.org/publicdomain/zero/1.0/">
		<img src="https://licensebuttons.net/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" /> </a>
	To the extent possible under law,
	<a rel="dct:publisher" href="https://joelkp.frama.io">
		<span property="dct:title">Joel K. Pettersson</span></a>
	has waived all copyright and related or neighboring rights to the
	<span property="dct:title" style="font-style:italic">example SAU scripts and rendered audio files</span>
	provided on this page and the <a href='#subpages'>examples subpages</a>. This work is published from:
	<span property="vcard:Country" datatype="dct:ISO3166"
		content="SE" about="https://joelkp.frama.io">Sweden</span>.
</p>
<p>To try to make that really clear, a script on this page appears as follows (assuming your web browser makes it stand out in any way)...
</p>
<pre>
/* Script contents go here. */
</pre>
<p>Also, it would be interesting to hear if you make good use of this stuff in some work &ndash; and feel free to point people here, though it's not required.
</p>
<footer>Text available under <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons BY-SA 4.0" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>.
</footer>
</body>
</html>
