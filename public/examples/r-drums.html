<!DOCTYPE html>
<html lang='en' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	<meta name='description'
content="R-oscillator drum examples collection for SAU (Scriptable AUdio) language." />
	<meta name='keywords'
content="drums, drumming, R oscillator, rumble, random line segments, saugns, SAU language, audio files" />
	<link rel='stylesheet' href='../style.css' type='text/css' title='Default' /><style type='text/css'>@import url("../style.css");</style>
<title>SAU R drum examples</title>
</head>
<body>
<nav><img src="../images/sau-logo88x50.png" alt=" " class='rightico' />
Back to <a href='../examples.html'>examples home</a>, or to <a href='../index.html'>main page</a>.
</nav>
<h1>SAU <code>R</code>-oscillator drum examples collection</h1>
<p>Here's the larger drum sound collection for the <a href='../language.html#rasg'><code>R</code> oscillator</a>. This is not exhaustive, but there's enough simple combinations of basic features to give it its own page.
</p>
<p>You can use these examples freely; the public domain dedication for all scripts and rendered audio files <a href='../examples.html#licensing'>on the main examples page</a> also applies to this subpage.
</p>
<div class='toc'>
<h2 id='contents'>Contents</h2>
<ul>
<li><a href='#R-drumming'>Drumming using <code>R</code></a><ul>
	<li><a href='#flatamp'>Randomized, no amplitude-shaping</a></li>
	<li><a href='#r-amp'>Non-random, with amplitude-shaping</a></li>
	</ul>
</li>
</ul>
</div>
<h2 id='R-drumming'>Drumming using <code>R</code></h2>
<p>There's several ways to use <code>R</code> instances for drumming, but the main possibilities come from using it for modulation. In particular, using an <code>Rxpe</code> PM modulator to make a carrier "beat" like a drum is a key ingredient in a range of simple recipes; apart from that modulator, the sound greatly depends on the choice of carrier. A more polished sound also usually depends on adding modulation of the amplitude to the carrier &ndash; for example using another <code>R</code> instance.
</p>
<p>Looking first at the <code>Rxpe</code> PM modulator, intensity and pace of drumming can be constant or be psuedo-randomly varied, by tweaking its mode, frequecy, and amplitude. For a fixed-pace drumbeat, fixed cycle mode <code>mf</code> or ternary smooth mode <code>mt</code> can be used &ndash; otherwise it will be pseudo-random drumming, which can be given a variety of simple patterns (varying the brightness or intensity, the timing, or both). More variations are also possible by using several PM modulators with different settings for the same drum.
</p>
<p>Technically, these drum sounds contain two different types of drum beat events which sound the same or similar. The PM modulator may rise (sharply at first), or it may fall (sharply at first) &ndash; and both will brighten the carrier's sound in the same way. If a <a href='language.html#wosc'><code>W</code> wave oscillator</a> is used as the carrier, with its frequency set to 0 Hz, it will sound the same &ndash; but with an <code>R</code> oscillator as the carrier, it will normally sound a bit different, because <code>R</code> is pseudo-periodic and each part of its signal makes for a somewhat different drum hit.
</p>
<p>For a start, here's a simple drum script with named variables for quantities like <abbr title="beats per minute">bpm</abbr> and for "strength" (intensity and brightness) of the drum sound. The carrier has a very low frequency, rather than a frequency of 0 Hz &ndash; this helps vary the sound a little over time, instead of making it alternate between the exact same back-and-forth beats all the time. Otherwise, the <code>f$bpm/(60*2)</code> could be moved in to the modulator, with <code>f0</code> added to the carrier. (A somewhat higher carrier frequency can also be used to produce a background hum or bassy noise which alternates with the drumbeats. For example, reusing the 'strength' variable, as in <code>f$strength</code>.)
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/ex-page/Rdrum-bpm.mp3' /></audio>
	<a href='../audio/ex-page/Rdrum-bpm.mp3'>MP3</a>
</div>
<pre>
$bpm=240
$strength=100
Rlin f$bpm/120 p[
        Rxpe mf a$strength*(120/$bpm)
] t30
</pre>
<p>It's a start, but without more it sounds a little rough in several ways. The subjective audio quality can be improved, among possibly other and much larger changes, by tweaking the carrier. Even then, without also adding amplitude modulation, such drum sounds have flat peak amplitude, the sense of intensity change coming from frequency change only &ndash; which usually sounds too soft or flat to be musically nice.
</p>
<h3 id='flatamp'>Randomized, no amplitude-shaping</h3>
<p>Below is a randomized version, with a little rumble in-between beats. Switching the <code>R</code> modulator to its default uniform random function mode, on average drumbeats seem to come less often &ndash; because beat intensity is sometimes too low to sound like a beat. Going beyond compensating for that, <em>the 'bpm' numbers are doubled below</em>, the bpm always reflecting the maximum rate (disregarding any missing beats, which also vary between the randomness modes which can be used).
</p>
<p>With this randomization, there's little gaps between the drum "hits" that stand out, adding to a weak or empty sound &ndash; but as one way to change that, below these <em>gaps are filled using a background bassy noise</em>, by setting a higher frequency for the carrier derived from the "strength" variable. (The modulator now has its frequency set directly, instead of implicitly based on that of the carrier.)
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/ex-page/Rdrum-bpm2-r.mp3' /></audio>
	<a href='../audio/ex-page/Rdrum-bpm2-r.mp3'>MP3</a>
</div>
<pre>
$bpm=480
$strength=100
Rlin mt f$strength/2 p[
	Rxpe f$bpm/120 a$strength*(240/$bpm)
] t30
</pre>
<p>This version also sounds less grainy; this is improved by using the ternary smooth random (<code>mt</code>) mode for the <code>R</code> <em>carrier</em>. Otherwise this randomized version would sound more, rather than less, rough.
</p>
<h4>Different carriers, different sounds</h4>
<p>The more grainy-sounding default random mode for the carrier is possible without making it sound terribly grainy if choosing a <em>very</em> smooth line type &ndash; <code>cos</code> &ndash; as in the below. The "strength" is boosted to compensate for the mellower sound of that line type. (More generally, moderate graininess is also possible by using the <code>mt</code> mode with a shaping level for a cross between it and uniform randomness.)
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/ex-page/Rdrum-bpm2-v.mp3' /></audio>
	<a href='../audio/ex-page/Rdrum-bpm2-v.mp3'>MP3</a>
</div>
<pre>
$bpm=480
$strength=150
Rcos f$strength/3 p[
        Rxpe mv f$bpm/120 a$strength*(240/$bpm)
] t30
</pre>
<p>Here the modulator also uses the basic violet random mode (by adding <code>mv</code>) &ndash; a somewhat smoother drumming pattern (reducing occasional "gaps" from very-low intensity hits) based on violet noise, compared to the default uniform random mode (which is based on white noise). The underlying randomness used determines the pattern. (Differences are even larger between the binary, ternary smooth, and uniform variations of white noise random as underlying noise.)
</p>
<p>Back to clean sounds, here's a sine-wave based chirp drum variation &ndash; too rudimentary a drum for use in a trance track, but thanks to the random pattern, not boring.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/ex-page/Rdrum-chirp-v.mp3' /></audio>
	<a href='../audio/ex-page/Rdrum-chirp-v.mp3'>MP3</a>
</div>
<pre>
$bpm=480
$strength=100
Wsin f$strength/3 p[
        Rxpe mv f$bpm/120 a$strength*(240/$bpm)
] t30
</pre>
<p>For a richer and messier, yet fairly gentle sound, here's a variation which uses a half-rectified sine (<code>Whsi</code>) for the carrier, and makes the modulator follow a random pattern based on Gaussian white noise (<code>mg</code>).
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/ex-page/Rdrum-rich-g.mp3' /></audio>
	<a href='../audio/ex-page/Rdrum-rich-g.mp3'>MP3</a>
</div>
<pre>
$bpm=480
$strength=75
Whsi f$strength/2 p1/12[
        Rxpe mg f$bpm/120 a$strength*(240/$bpm)
] t30
</pre>
<p>Finally &ndash; for now &ndash; let's go for one of the most intense and noisy drum sounds <code>R</code> can offer through a simple script. The line type <code>sah</code> produces a squarish wave, and both the tempo and the strength have been cranked up; the carrier again uses <code>mt</code> to keep the sound reasonably clear (as opposed to more distorted and crackly). The modulator uses the violet binary mode <code>mvb</code>, and so shifts rapidly and chaotically between two paces (half and full) while keeping each beat equally strong.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/ex-page/Rdrum-noisy-vb.mp3' /></audio>
	<a href='../audio/ex-page/Rdrum-noisy-vb.mp3'>MP3</a>
</div>
<pre>
$bpm=600
$strength=125
Rsah mt f$strength*(2/7) p[
	Rxpe mvb f$bpm/120 a$strength*(240/$bpm)
] t30
</pre>
<p>Different and more numerous variations to the carrier can sound decent when also shaping the amplitude, as in the below.
</p>
<h3 id='r-amp'>Non-random, with amplitude-shaping</h3>
<p>With amplitude shaping, simple drum sounds without randomization can sound good to listen to. The various wave types available for the <code>W</code> oscillator then make for interesting variations for the carrier. These sounds are more intense and use lower "strength" values to compensate, compared to those above. There's also less of a background rumble, as the amplitude shaping consists in sucking down the level in some way to create peaks.
</p>
<h4>EDM drums with imitated dynamics compression</h4>
<p>The <code>Rsqe</code> amplitude modulator below is used in a way that makes for a "ducking" sound similar to that from compression, for pairs of strong and weak beats &ndash; the second sub-beat fading in from a low amplitude &ndash; like in some prominent 1990s techno and trance. But the carrier's evenangle wave type is an unusual choice. (Music I've listened to has often had drums with the sound of sine or triangle waves.) Note that with these strong&ndash;weak beat pairs, the pace sounds like <em>half the bpm technically used</em>.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/drums/r-ean-drum-duck.mp3' /></audio>
	<a href='../audio/drums/r-ean-drum-duck.mp3'>MP3</a>
</div>
<pre>
$bpm=120*2
$strength=20
Wean f$strength/3 p6/93[
        Rxpe mf f$bpm/120 a$strength*(240/$bpm)
] a1/8.r1[
        Rsqe mf f$bpm/120
] t30
</pre>
<p>With such strong&ndash;weak beat pairs, it's often nice to vary the frequency used for each beat in the pair. This can be done by adding another inner modulator for the PM modulator that does the "beating", to vary its amplitude. Here's an example of a triangle wave-based drum, where the first sub-beat has a lower frequency than the second &ndash; the <code>p1/2</code> phase offset of the new inner <code>Rcos</code> modulator, if changed, moves the frequency top and bottom within each beat-pair. (Why a sinuous inner modulator? It's a smooth way to step between two values; a squarish jump wouldn't sound clean here.)
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/drums/r-tri-drum-pair.mp3' /></audio>
	<a href='../audio/drums/r-tri-drum-pair.mp3'>MP3</a>
</div>
<pre>
$bpm=120*2
$strength=30
Wtri f0 p[
        Rxpe mf f$bpm/120 a$strength*(240/$bpm)/2.r$strength*(240/$bpm)[
		Rcos mf p1/2
	]
] a1/8.r1[
        Rsqe mf f$bpm/120
] t30
</pre>
<p>Beyond this, for each choice of carrier, additional variation to color and brightness can be created e.g. by adding an extra LFO to tweak the carrier frequency value. A sawtooth LFO (as below) gives a straightforward effect that often sounds nice, though other wave types can create different effects.
</p>
<p>For more "bright" or intense carrier waveform choices, the simply emulated compression "sound" can become a bit too much, a bit annoying &ndash; this can be avoided by either just reducing the <a href='../language.html#modvalrange'>AM depth</a>, by making the main <code>a</code> value closer to 1, or by setting the line type of the amplitude modulator to <code>cub</code> instead. Compared to the <code>sqe</code> line, <code>cub</code> makes the level spend more time in the middle of the range instead of just going up and down.
</p>
<p>Here's a script with those changes, using the medium-bright square root of sine wave type for the carrier &ndash; for a brighter, harder drum in-between triangle and square-based sounds &ndash; and at a higher pace. This script is good for a start in experimenting with simple drum patterns. The carrier wave type can also simply be changed and it'll sound pretty good for other types, too.
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/drums/r-srs-drum-lfo.mp3' /></audio>
	<a href='../audio/drums/r-srs-drum-lfo.mp3'>MP3</a>
</div>
<pre>
$bpm=150*2
$strength=30
Wsrs f0[
        Wsaw f$bpm/240 a$strength*2
] p[
        Rxpe mf f$bpm/120 a$strength*(240/$bpm)/2.r$strength*(240/$bpm)[
                Rcos mf p1/2
        ]
] a1/8.r1[
        Rcub mf f$bpm/120
] t30
</pre>
<p>It's possible to change things up in more ways. Above, a sinuous transition was used to vary the frequency reached by beats, to set apart the low and high sub-beats, by using an inner <code>Rcos</code> oscillator &ndash; but more sounds are possible using various waveforms in the role of that modulator. A <code>W</code> oscillator can be used just as well (and if phase is lined up with a cosine wave by adding <code>p1/4</code>, the phase will match that of an <code>Rcos</code> oscillator).
</p>
<p>Here's another variation &ndash; this time with a catear medium-bright wave type for the carrier &ndash; where different amplitude shaping combines with swapping in an evenangle inner modulator for the varying of the frequencies the two sub-beats reach, giving the beats another sound. An eventooth LFO is also used alongside the sawtooth to glide around the pitch a little differently; varying their parameters further can be interesting. This one also sounds nice for other carrier wave types.
</p>
<div class="audiofile">
	<audio controls preload='metadata'>
		<source src='../audio/drums/r-cat-drum-lfo.mp3' /></audio>
	<a href='../audio/drums/r-cat-drum-lfo.mp3'>MP3</a>
</div>
<pre>
$bpm=150*2
$strength=30
Wcat f0[
        Wsaw f$bpm/240 a$strength p1/2
        Weto f$bpm/240 a$strength
] p[
        Rxpe mf f$bpm/120 a$strength*(240/$bpm)/2.r$strength*(240/$bpm)[
                Wean p3/4
        ]
] a1/4.r1[
        Rsqe mf f$bpm/120
] t30
</pre>
<footer>Text available under <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons BY-SA 4.0" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>.
</footer>
</body>
</html>
